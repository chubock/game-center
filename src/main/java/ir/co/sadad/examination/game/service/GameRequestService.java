package ir.co.sadad.examination.game.service;

import ir.co.sadad.examination.game.entity.GameDetail;
import ir.co.sadad.examination.game.entity.GameRequest;
import ir.co.sadad.examination.game.entity.GameType;
import ir.co.sadad.examination.game.entity.User;
import ir.co.sadad.examination.game.exception.*;
import ir.co.sadad.examination.game.model.GameRequestModel;
import ir.co.sadad.examination.game.repository.GameDetailRepository;
import ir.co.sadad.examination.game.repository.GameRequestRepository;
import ir.co.sadad.examination.game.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class GameRequestService {

    private final GameRequestRepository gameRequestRepository;
    private final UserRepository userRepository;
    private final GameDetailRepository gameDetailRepository;

    public GameRequestService(GameRequestRepository gameRequestRepository,
                              UserRepository userRepository,
                              GameDetailRepository gameDetailRepository) {

        this.gameRequestRepository = gameRequestRepository;
        this.userRepository = userRepository;
        this.gameDetailRepository = gameDetailRepository;
    }

    @Transactional
    public GameRequestModel createGameRequest(String username, GameType gameType) {

        User requester = userRepository.findByUsername(username)
                .orElseThrow(UserNotFoundException::new);

        GameDetail gameDetail = gameDetailRepository.findById(gameType)
                .orElseThrow(GameDetailNotFoundException::new);

        if (requester.getCredit() < gameDetail.getPrice())
            throw new UserCreditException();

        requester.reduceCredit(gameDetail.getPrice());

        GameRequest gameRequest = new GameRequest(requester, gameType);
        gameRequestRepository.save(gameRequest);

        return GameRequestModel.valueOf(gameRequest);

    }

    @Transactional
    public void closeGameRequest(String username, Long gameRequestId) {

        GameRequest gameRequest = gameRequestRepository.findByIdWithRequester(gameRequestId)
                .orElseThrow(GameRequestNotFoundException::new);

        if (! username.equals(gameRequest.getRequester().getUsername()))
            throw new GameRequestNotFoundException();

        if (gameRequest.getState() != GameRequest.State.OPEN)
            throw new InvalidGameRequestException();

        GameDetail gameDetail = gameDetailRepository.findById(gameRequest.getGameType())
                .orElseThrow(BadRequestException::new);

        gameRequest.getRequester().addCredit(gameDetail.getPrice());

        gameRequest.close();
    }

}
