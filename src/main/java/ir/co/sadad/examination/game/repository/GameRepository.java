package ir.co.sadad.examination.game.repository;

import ir.co.sadad.examination.game.entity.Game;
import ir.co.sadad.examination.game.entity.GameType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface GameRepository extends JpaRepository<Game, Long> {

    @Query("select g.gameType from Game g where g.id = :id")
    Optional<GameType> selectGameTypeById(@Param("id") Long id);

}
