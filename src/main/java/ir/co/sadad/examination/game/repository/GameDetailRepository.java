package ir.co.sadad.examination.game.repository;

import ir.co.sadad.examination.game.entity.GameDetail;
import ir.co.sadad.examination.game.entity.GameType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameDetailRepository extends JpaRepository<GameDetail, GameType> {



}
