package ir.co.sadad.examination.game.model;

import ir.co.sadad.examination.game.entity.GameRequest;
import ir.co.sadad.examination.game.entity.GameType;

public class GameRequestModel {

    private Long id;
    private String requester;
    private GameType gameType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequester() {
        return requester;
    }

    public void setRequester(String requester) {
        this.requester = requester;
    }

    public GameType getGameType() {
        return gameType;
    }

    public void setGameType(GameType gameType) {
        this.gameType = gameType;
    }

    public static GameRequestModel valueOf(GameRequest gameRequest) {
        if (gameRequest == null)
            return null;
        GameRequestModel model = new GameRequestModel();
        model.setId(gameRequest.getId());
        if (gameRequest.getRequester() != null)
            model.setRequester(gameRequest.getRequester().getUsername());
        model.setGameType(gameRequest.getGameType());
        return model;
    }
}
