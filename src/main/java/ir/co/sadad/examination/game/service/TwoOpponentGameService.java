package ir.co.sadad.examination.game.service;

import ir.co.sadad.examination.game.entity.GameDetail;
import ir.co.sadad.examination.game.entity.GameRequest;
import ir.co.sadad.examination.game.entity.TwoOpponentGame;
import ir.co.sadad.examination.game.entity.User;
import ir.co.sadad.examination.game.exception.BadRequestException;
import ir.co.sadad.examination.game.exception.GameNotFoundException;
import ir.co.sadad.examination.game.exception.InvalidGameException;
import ir.co.sadad.examination.game.exception.SystemException;
import ir.co.sadad.examination.game.model.TwoOpponentGameModel;
import ir.co.sadad.examination.game.repository.GameDetailRepository;
import ir.co.sadad.examination.game.repository.GameRequestRepository;

import javax.transaction.Transactional;
import java.util.Optional;

public abstract class TwoOpponentGameService<T extends TwoOpponentGameModel, E extends TwoOpponentGame> implements GameService<T> {

    private final GameRequestRepository gameRequestRepository;
    private final GameDetailRepository gameDetailRepository;


    public TwoOpponentGameService(GameRequestRepository gameRequestRepository,
                                  GameDetailRepository gameDetailRepository) {
        this.gameRequestRepository = gameRequestRepository;
        this.gameDetailRepository = gameDetailRepository;
    }

    @Transactional
    public Optional<T> create(Long gameRequestId) {

        GameRequest gameRequest = gameRequestRepository.findByIdWithRequester(gameRequestId)
                .orElseThrow(SystemException::new);

        GameDetail gameDetail = gameDetailRepository.findById(gameRequest.getGameType())
                .orElseThrow(BadRequestException::new);

        return gameRequestRepository.findFirstByRequesterIsNotAndGameTypeAndState(gameRequest.getRequester(), gameRequest.getGameType(), GameRequest.State.OPEN)
                .map(foundGameRequest -> create(gameRequest, foundGameRequest));

    }

    @Transactional
    public T move(Long gameId, String username, String move) {

        E game = get(gameId)
                .orElseThrow(GameNotFoundException::new);

        if (game.isFinished())
            throw new InvalidGameException();

        if (game.getFirstOpponent().getUsername().equals(username))
            game.firstOpponentMove(move);
        else if (game.getSecondOpponent().getUsername().equals(username))
            game.secondOpponentMove(move);
        else
            throw new GameNotFoundException();

        if (game.isFinished()) {

            GameDetail gameDetail = gameDetailRepository.findById(game.getGameType())
                    .orElseThrow(SystemException::new);

            if (game.getWinner() != null)
                game.getWinner().addCredit(gameDetail.getWinnerPrice());
            else {
                game.getFirstOpponent().addCredit(gameDetail.getWinnerPrice() / 2);
                game.getSecondOpponent().addCredit(gameDetail.getWinnerPrice() / 2);
            }
        }

        return modelOf(game);

    }

    private T create(GameRequest first, GameRequest second) {

        GameDetail gameDetail = gameDetailRepository.findById(first.getGameType())
                .orElseThrow(SystemException::new);


        E game = create(first.getRequester(), second.getRequester(), gameDetail.getPrice());

        first.done(game);
        second.done(game);

        return modelOf(game);

    }

    abstract Optional<E> get(Long id);
    abstract E create(User firstOpponent, User secondOpponent, Integer price);
    abstract T modelOf(E game);

}
