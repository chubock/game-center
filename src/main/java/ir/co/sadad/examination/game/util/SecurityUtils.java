package ir.co.sadad.examination.game.util;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

public class SecurityUtils {

    private static UserDetails getCurrentUserDetails() {
        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return (User) principal;
        } catch (Exception e) {
            throw new SecurityException();
        }
    }

    public static String getCurrentUsername() {
        return getCurrentUserDetails().getUsername();
    }


}
