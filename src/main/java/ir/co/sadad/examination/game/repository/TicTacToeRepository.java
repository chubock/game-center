package ir.co.sadad.examination.game.repository;

import ir.co.sadad.examination.game.entity.TicTacToe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TicTacToeRepository extends JpaRepository<TicTacToe, Long> {
}
