package ir.co.sadad.examination.game;

import ir.co.sadad.examination.game.entity.GameDetail;
import ir.co.sadad.examination.game.entity.GameType;
import ir.co.sadad.examination.game.entity.User;
import ir.co.sadad.examination.game.repository.GameDetailRepository;
import ir.co.sadad.examination.game.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.transaction.Transactional;

@SpringBootApplication
public class GameApplication implements CommandLineRunner {

	private final GameDetailRepository gameDetailRepository;
	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;

	public GameApplication(GameDetailRepository gameDetailRepository, UserRepository userRepository, PasswordEncoder passwordEncoder) {
		this.gameDetailRepository = gameDetailRepository;
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
	}

	public static void main(String[] args) {
		SpringApplication.run(GameApplication.class, args);
	}

	@Override
	@Transactional
	public void run(String... args) throws Exception {
		gameDetailRepository.save(new GameDetail(GameType.ROCK_PAPER_SCISSOR, 10, 8));
		gameDetailRepository.save(new GameDetail(GameType.TIC_TAC_TOE, 20, 16));
		userRepository.save(new User("player1", passwordEncoder.encode("1234"), 1000L));
		userRepository.save(new User("player2", passwordEncoder.encode("1234"), 1000L));
	}
}
