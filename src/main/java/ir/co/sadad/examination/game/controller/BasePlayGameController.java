package ir.co.sadad.examination.game.controller;

import ir.co.sadad.examination.game.model.GameModel;
import ir.co.sadad.examination.game.service.GameService;
import ir.co.sadad.examination.game.util.SecurityUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

public class BasePlayGameController<T extends GameModel> {

    private final GameService<T> gameService;

    BasePlayGameController(GameService<T> gameService) {
        this.gameService = gameService;
    }

    @PatchMapping
    @ResponseStatus(value = HttpStatus.OK)
    public void playGame(@PathVariable("id") Long gameId, @Valid @RequestBody Request request) {
        T gameModel = gameService.move(gameId, SecurityUtils.getCurrentUsername(), request.move);
        afterGamePlayedHandler(request.move, gameModel);
    }

    void afterGamePlayedHandler(String move, T gameModel) {}

    static class Request {

        @NotEmpty
        private String move;

        public String getMove() {
            return move;
        }

        public void setMove(String move) {
            this.move = move;
        }
    }

}
