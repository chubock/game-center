package ir.co.sadad.examination.game.message;

import ir.co.sadad.examination.game.model.TwoOpponentGameModel;

public class TwoOpponentGameFinishedMessage extends GameFinishedMessage {

    private String firstOpponent;
    private String secondOpponent;
    private String winner;

    public String getFirstOpponent() {
        return firstOpponent;
    }

    public void setFirstOpponent(String firstOpponent) {
        this.firstOpponent = firstOpponent;
    }

    public String getSecondOpponent() {
        return secondOpponent;
    }

    public void setSecondOpponent(String secondOpponent) {
        this.secondOpponent = secondOpponent;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    TwoOpponentGameFinishedMessage fill(TwoOpponentGameModel model) {
        super.fill(model);
        setFirstOpponent(model.getFirstOpponent());
        setSecondOpponent(model.getSecondOpponent());
        setWinner(model.getWinner());
        return this;
    }

    public static TwoOpponentGameFinishedMessage messageOf(TwoOpponentGameModel model) {
        if (model == null)
            return null;
        TwoOpponentGameFinishedMessage message = new TwoOpponentGameFinishedMessage();
        return message.fill(model);
    }
}
