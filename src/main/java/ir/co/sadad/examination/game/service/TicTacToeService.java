package ir.co.sadad.examination.game.service;

import ir.co.sadad.examination.game.entity.TicTacToe;
import ir.co.sadad.examination.game.entity.User;
import ir.co.sadad.examination.game.model.TicTacToeModel;
import ir.co.sadad.examination.game.repository.GameDetailRepository;
import ir.co.sadad.examination.game.repository.GameRequestRepository;
import ir.co.sadad.examination.game.repository.TicTacToeRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TicTacToeService extends TwoOpponentGameService<TicTacToeModel, TicTacToe> {

    private final TicTacToeRepository ticTacToeRepository;

    public TicTacToeService(GameRequestRepository gameRequestRepository,
                            GameDetailRepository gameDetailRepository,
                            TicTacToeRepository ticTacToeRepository) {
        super(gameRequestRepository, gameDetailRepository);
        this.ticTacToeRepository = ticTacToeRepository;
    }

    @Override
    Optional<TicTacToe> get(Long id) {
        return ticTacToeRepository.findById(id);
    }

    @Override
    TicTacToe create(User firstOpponent, User secondOpponent, Integer price) {

        TicTacToe ticTacToe = new TicTacToe(firstOpponent, secondOpponent, price);
        return ticTacToeRepository.save(ticTacToe);

    }

    @Override
    TicTacToeModel modelOf(TicTacToe game) {
        return TicTacToeModel.valueOf(game);
    }
}
