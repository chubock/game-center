package ir.co.sadad.examination.game.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
public class GameRequest implements Serializable {

    private Long id;
    private User requester;
    private GameType gameType;
    private State state = State.OPEN;
    private Game game;

    protected GameRequest() {
    }

    public GameRequest(User requester, GameType gameType) {
        setRequester(requester);
        setGameType(gameType);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    private void setId(Long id) {
        this.id = id;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    public User getRequester() {
        return requester;
    }

    private void setRequester(User requester) {
        if (requester == null)
            throw new NullPointerException();
        this.requester = requester;
    }

    @NotNull
    @Enumerated
    public GameType getGameType() {
        return gameType;
    }

    private void setGameType(GameType gameDetail) {
        if (gameDetail == null)
            throw new NullPointerException();
        this.gameType = gameDetail;
    }

    @NotNull
    @Enumerated
    public State getState() {
        return state;
    }

    private void setState(State state) {
        this.state = state;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Game getGame() {
        return game;
    }

    private void setGame(Game game) {
        this.game = game;
    }

    public void close(){
        if (state != State.OPEN)
            throw new IllegalStateException();
        setState(State.CLOSED);
    }

    public void done(Game game) {
        if (state != State.OPEN)
            throw new IllegalStateException();
        if (game == null)
            throw new NullPointerException();
        if (game.getGameType() != gameType)
            throw new IllegalArgumentException();
        setState(State.DONE);
        setGame(game);
    }

    public enum State {
        OPEN, CLOSED, DONE
    }
}
