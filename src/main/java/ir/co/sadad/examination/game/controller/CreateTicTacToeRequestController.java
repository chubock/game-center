package ir.co.sadad.examination.game.controller;

import ir.co.sadad.examination.game.entity.GameType;
import ir.co.sadad.examination.game.message.TicTacToeCreatedMessage;
import ir.co.sadad.examination.game.model.TicTacToeModel;
import ir.co.sadad.examination.game.service.GameRequestService;
import ir.co.sadad.examination.game.service.TicTacToeService;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/tic-tac-toe-requests")
public class CreateTicTacToeRequestController extends BaseCreateGameRequestController<TicTacToeModel> {

    private final SimpMessagingTemplate simpMessagingTemplate;

    public CreateTicTacToeRequestController(GameRequestService gameRequestService,
                                            TicTacToeService ticTacToeService,
                                            SimpMessagingTemplate simpMessagingTemplate) {
        super(gameRequestService, ticTacToeService, GameType.TIC_TAC_TOE);
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @Override
    void afterGameCreatedHandler(TicTacToeModel gameModel) {
        TicTacToeCreatedMessage message = TicTacToeCreatedMessage.messageOf(gameModel);
        gameModel.getParticipants()
                .forEach(username -> simpMessagingTemplate.convertAndSendToUser(username, "/queue/reply", message));
    }
}
