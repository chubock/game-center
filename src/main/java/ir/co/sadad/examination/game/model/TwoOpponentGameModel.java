package ir.co.sadad.examination.game.model;

import ir.co.sadad.examination.game.entity.TwoOpponentGame;

import java.util.Arrays;
import java.util.List;

public class TwoOpponentGameModel extends GameModel {

    private String firstOpponent;
    private String secondOpponent;
    private String winner;

    public String getFirstOpponent() {
        return firstOpponent;
    }

    public void setFirstOpponent(String firstOpponent) {
        this.firstOpponent = firstOpponent;
    }

    public String getSecondOpponent() {
        return secondOpponent;
    }

    public void setSecondOpponent(String secondOpponent) {
        this.secondOpponent = secondOpponent;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    @Override
    public List<String> getParticipants() {
        return Arrays.asList(firstOpponent, secondOpponent);
    }

    void fill(TwoOpponentGame game) {
        super.fill(game);
        setFirstOpponent(game.getFirstOpponent().getUsername());
        setSecondOpponent(game.getSecondOpponent().getUsername());
        if (game.getWinner() != null)
            setWinner(game.getWinner().getUsername());
    }

    public static TwoOpponentGameModel valueOf(TwoOpponentGame game) {
        if (game == null)
            return null;
        TwoOpponentGameModel model = new TwoOpponentGameModel();
        model.fill(game);
        return model;
    }
}
