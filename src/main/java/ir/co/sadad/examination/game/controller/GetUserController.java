package ir.co.sadad.examination.game.controller;

import ir.co.sadad.examination.game.model.UserModel;
import ir.co.sadad.examination.game.service.UserService;
import ir.co.sadad.examination.game.util.SecurityUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetUserController {

    private final UserService userService;

    public GetUserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/me")
    public UserModel getUser() {
        return userService.getUser(SecurityUtils.getCurrentUsername());
    }
}
