package ir.co.sadad.examination.game.service;

import ir.co.sadad.examination.game.exception.UserNotFoundException;
import ir.co.sadad.examination.game.model.UserModel;
import ir.co.sadad.examination.game.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    public UserModel getUser(String username) {
        return userRepository.findByUsername(username)
                .map(UserModel::valueOf)
                .orElseThrow(UserNotFoundException::new);
    }
}
