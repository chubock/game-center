package ir.co.sadad.examination.game.controller;

import ir.co.sadad.examination.game.entity.GameType;
import ir.co.sadad.examination.game.model.GameModel;
import ir.co.sadad.examination.game.model.GameRequestModel;
import ir.co.sadad.examination.game.service.GameRequestService;
import ir.co.sadad.examination.game.service.GameService;
import ir.co.sadad.examination.game.util.SecurityUtils;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.concurrent.CompletableFuture;

public class BaseCreateGameRequestController<T extends GameModel> {

    private final GameRequestService gameRequestService;
    private final GameService<T> gameService;
    private final GameType gameType;

    BaseCreateGameRequestController(GameRequestService gameRequestService, GameService<T> gameService, GameType gameType) {
        this.gameRequestService = gameRequestService;
        this.gameService = gameService;
        this.gameType = gameType;
    }

    @PostMapping
    public GameRequestModel createGameRequest() {
        GameRequestModel model = gameRequestService.createGameRequest(SecurityUtils.getCurrentUsername(), gameType);
        gameService.create(model.getId())
                .ifPresent(gameModel -> CompletableFuture.runAsync(() -> afterGameCreatedHandler(gameModel)));
        return model;
    }

    void afterGameCreatedHandler(T gameModel) {}

}
