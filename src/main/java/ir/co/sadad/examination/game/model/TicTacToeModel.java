package ir.co.sadad.examination.game.model;

import ir.co.sadad.examination.game.entity.TicTacToe;

public class TicTacToeModel extends TwoOpponentGameModel {

    private Integer firstOpponentChoices;
    private Integer secondOpponentChoices;
    private String opponentTurn;

    public Integer getFirstOpponentChoices() {
        return firstOpponentChoices;
    }

    public void setFirstOpponentChoices(Integer firstOpponentChoices) {
        this.firstOpponentChoices = firstOpponentChoices;
    }

    public Integer getSecondOpponentChoices() {
        return secondOpponentChoices;
    }

    public void setSecondOpponentChoices(Integer secondOpponentChoices) {
        this.secondOpponentChoices = secondOpponentChoices;
    }

    public String getOpponentTurn() {
        return opponentTurn;
    }

    public void setOpponentTurn(String opponentTurn) {
        this.opponentTurn = opponentTurn;
    }

    void fill(TicTacToe game) {
        super.fill(game);
        setFirstOpponentChoices(game.getFirstOpponentChoices());
        setSecondOpponentChoices(game.getSecondOpponentChoices());
        setOpponentTurn(game.getOpponentTurn().getUsername());
    }

    public static TicTacToeModel valueOf(TicTacToe game) {
        if (game == null)
            return null;
        TicTacToeModel model = new TicTacToeModel();
        model.fill(game);
        return model;
    }


}
