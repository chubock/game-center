package ir.co.sadad.examination.game.controller;

import ir.co.sadad.examination.game.message.RockPaperScissorFinishedMessage;
import ir.co.sadad.examination.game.model.RockPaperScissorModel;
import ir.co.sadad.examination.game.service.RockPaperScissorService;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/games/rock-paper-scissors/{id}")
public class PlayRockPaperScissorController extends BasePlayGameController<RockPaperScissorModel> {

    private final SimpMessagingTemplate simpMessagingTemplate;

    public PlayRockPaperScissorController(RockPaperScissorService rockPaperScissorService,
                                          SimpMessagingTemplate simpMessagingTemplate) {
        super(rockPaperScissorService);
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @Override
    void afterGamePlayedHandler(String move, RockPaperScissorModel gameModel) {

        if (gameModel.isFinished()) {

            RockPaperScissorFinishedMessage message = RockPaperScissorFinishedMessage.messageOf(gameModel);

            gameModel.getParticipants()
                    .forEach(username -> simpMessagingTemplate.convertAndSendToUser(username, "/queue/reply", message));

        }

    }
}
