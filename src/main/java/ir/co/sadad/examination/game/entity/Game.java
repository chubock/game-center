package ir.co.sadad.examination.game.entity;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Game implements Serializable {

    private Long id;
    private Integer price;
    private State state = State.STARTED;
    private Date startedDate = new Date();
    private Date finishedDate;

    protected Game() {
    }

    public Game(Integer price) {
        setPrice(price);
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    private void setId(Long id) {
        this.id = id;
    }

    @NotNull
    @Min(0)
    public Integer getPrice() {
        return price;
    }

    private void setPrice(Integer price) {
        if (price == null)
            throw new NullPointerException();
        if (price < 0)
            throw new IllegalArgumentException();
        this.price = price;
    }

    @NotNull
    @Enumerated
    public State getState() {
        return state;
    }

    private void setState(State state) {
        if (state == null)
            throw new NullPointerException();
        this.state = state;
    }

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    public Date getStartedDate() {
        return startedDate;
    }

    private void setStartedDate(Date startedDate) {
        if (startedDate == null)
            throw new NullPointerException();
        this.startedDate = startedDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    public Date getFinishedDate() {
        return finishedDate;
    }

    private void setFinishedDate(Date finishedDate) {
        this.finishedDate = finishedDate;
    }

    @NotNull
    @Enumerated
    @Column(updatable = false)
    abstract public GameType getGameType();
    private void setGameType(GameType gameType) {}

    void finish() {
        setState(State.FINISHED);
        setFinishedDate(new Date());
    }

    @Transient
    public Boolean isFinished() {
        return getState() == State.FINISHED;
    }

    enum State {
        STARTED, FINISHED
    }
}
