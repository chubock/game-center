package ir.co.sadad.examination.game.model;

import ir.co.sadad.examination.game.entity.Game;
import ir.co.sadad.examination.game.entity.GameType;

import java.util.List;

public abstract class GameModel {

    private Long id;
    private Integer price;
    private GameType gameType;
    private Boolean finished;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public GameType getGameType() {
        return gameType;
    }

    public void setGameType(GameType gameType) {
        this.gameType = gameType;
    }

    public Boolean isFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }

    public abstract List<String> getParticipants();

    void fill (Game game) {
        setId(game.getId());
        setPrice(game.getPrice());
        setGameType(game.getGameType());
        setFinished(game.isFinished());
    }
}
