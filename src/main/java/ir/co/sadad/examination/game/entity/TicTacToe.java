package ir.co.sadad.examination.game.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Entity
public class TicTacToe extends TwoOpponentGame {

    private Integer firstOpponentChoices = 0;
    private Integer secondOpponentChoices = 0;
    private User opponentTurn;


    protected TicTacToe() {
    }

    public TicTacToe(User firstOpponent, User secondOpponent, Integer price) {
        super(firstOpponent, secondOpponent, price);
        setOpponentTurn(firstOpponent);
    }

    @NotNull
    @PositiveOrZero
    public Integer getFirstOpponentChoices() {
        return firstOpponentChoices;
    }

    private void setFirstOpponentChoices(Integer firstOpponentChoices) {
        this.firstOpponentChoices = firstOpponentChoices;
    }

    @NotNull
    @PositiveOrZero
    public Integer getSecondOpponentChoices() {
        return secondOpponentChoices;
    }

    private void setSecondOpponentChoices(Integer secondOpponentChoices) {
        this.secondOpponentChoices = secondOpponentChoices;
    }

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    public User getOpponentTurn() {
        return opponentTurn;
    }

    private void setOpponentTurn(User opponentTurn) {
        this.opponentTurn = opponentTurn;
    }

    @Override
    public void firstOpponentMove(String move) {

        if (getSecondOpponent().equals(getOpponentTurn()))
            throw new IllegalStateException();

        if (!validMove(move))
            throw new IllegalArgumentException();

        firstOpponentChoices = firstOpponentChoices | getPower(move);

        setOpponentTurn(getSecondOpponent());

        if (isWinner(firstOpponentChoices))
            win(getFirstOpponent());
        else if (checkDraw())
            draw();

    }

    @Override
    public void secondOpponentMove(String move) {

        if (getFirstOpponent().equals(getOpponentTurn()))
            throw new IllegalStateException();

        if (!validMove(move))
            throw new IllegalArgumentException();

        secondOpponentChoices = secondOpponentChoices | getPower(move);

        setOpponentTurn(getFirstOpponent());

        if (isWinner(secondOpponentChoices))
            win(getSecondOpponent());
        else if (checkDraw())
            draw();

    }

    private boolean checkDraw() {
        return (firstOpponentChoices | secondOpponentChoices) == 0b111111111;
    }

    private boolean validMove(String move) {
        Integer pow = getPower(move);
        return Integer.valueOf(move) < 10 && (firstOpponentChoices & pow) == 0 && (secondOpponentChoices & pow) == 0;
    }

    private Integer getPower(String move) {
        return Double.valueOf(Math.pow(2, Double.valueOf(move))).intValue();
    }

    private boolean isWinner(Integer choices) {
        return (choices & 0b111) == 0b111 ||
               (choices & 0b111000) == 0b111000 ||
               (choices & 0b111000000) == 0b111000000 ||
               (choices & 0b001001001) == 0b001001001 ||
               (choices & 0b010010010) == 0b010010010 ||
               (choices & 0b100100100) == 0b100100100 ||
               (choices & 0b100010001) == 0b100010001 ||
               (choices & 0b001010100) == 0b001010100;
    }

    @Override
    public @NotNull GameType getGameType() {
        return GameType.TIC_TAC_TOE;
    }
}
