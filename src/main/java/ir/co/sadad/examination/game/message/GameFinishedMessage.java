package ir.co.sadad.examination.game.message;

import ir.co.sadad.examination.game.model.GameModel;

public class GameFinishedMessage {

    private String messageType = "GAME_FINISHED";
    private Long gameId;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    GameFinishedMessage fill(GameModel model) {
        setGameId(model.getId());
        return this;
    }

    public static GameFinishedMessage messageOf(GameModel model) {
        if (model == null)
            return null;
        GameFinishedMessage message = new GameFinishedMessage();
        return message.fill(model);
    }
}
