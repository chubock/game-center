package ir.co.sadad.examination.game.repository;

import ir.co.sadad.examination.game.entity.TwoOpponentGame;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TwoOpponentGameRepository extends JpaRepository<TwoOpponentGame, Long> {
}
