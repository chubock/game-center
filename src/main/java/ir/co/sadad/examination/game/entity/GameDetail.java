package ir.co.sadad.examination.game.entity;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
public class GameDetail implements Serializable {

    private GameType type;
    private Integer price;
    private Integer winnerPrice;

    protected GameDetail() {
    }

    public GameDetail(GameType type, Integer price, Integer winnerPrice) {
        this.type = type;
        setPrice(price);
        setWinnerPrice(winnerPrice);
    }

    @Id
    @Enumerated
    public GameType getType() {
        return type;
    }

    private void setType(GameType type) {
        if (type == null)
            throw new NullPointerException();
        this.type = type;
    }

    @NotNull
    @Min(0)
    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        if (price == null)
            throw new NullPointerException();
        if (price < 0)
            throw new IllegalArgumentException();
        this.price = price;
    }

    @NotNull
    @Min(0)
    public Integer getWinnerPrice() {
        return winnerPrice;
    }

    public void setWinnerPrice(Integer winnerPrice) {
        if (price == null)
            throw new NullPointerException();
        if (price < 0)
            throw new IllegalArgumentException();
        this.winnerPrice = winnerPrice;
    }
}
