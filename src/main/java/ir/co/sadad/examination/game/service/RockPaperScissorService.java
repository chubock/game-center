package ir.co.sadad.examination.game.service;

import ir.co.sadad.examination.game.entity.RockPaperScissor;
import ir.co.sadad.examination.game.entity.User;
import ir.co.sadad.examination.game.model.RockPaperScissorModel;
import ir.co.sadad.examination.game.repository.GameDetailRepository;
import ir.co.sadad.examination.game.repository.GameRequestRepository;
import ir.co.sadad.examination.game.repository.RockPaperScissorRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RockPaperScissorService extends TwoOpponentGameService<RockPaperScissorModel, RockPaperScissor> {

    private final RockPaperScissorRepository rockPaperScissorRepository;

    public RockPaperScissorService(GameRequestRepository gameRequestRepository,
                                   GameDetailRepository gameDetailRepository,
                                   RockPaperScissorRepository rockPaperScissorRepository) {
        super(gameRequestRepository, gameDetailRepository);
        this.rockPaperScissorRepository = rockPaperScissorRepository;
    }

    @Override
    Optional<RockPaperScissor> get(Long id) {
        return rockPaperScissorRepository.findById(id);
    }

    @Override
    RockPaperScissor create(User firstOpponent, User secondOpponent, Integer price) {

        RockPaperScissor rockPaperScissor = new RockPaperScissor(firstOpponent, secondOpponent, price);
        return rockPaperScissorRepository.save(rockPaperScissor);

    }

    @Override
    RockPaperScissorModel modelOf(RockPaperScissor game) {
        return RockPaperScissorModel.valueOf(game);
    }
}
