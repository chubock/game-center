package ir.co.sadad.examination.game.message;

import ir.co.sadad.examination.game.model.GameModel;

public class GameCreatedMessage {
    private String messageType = "GAME_CREATED";
    private Long id;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    GameCreatedMessage fill(GameModel gameModel) {
        setId(gameModel.getId());
        return this;
    }

    public static GameCreatedMessage messageOf(GameModel model) {
        if (model == null)
            return null;
        GameCreatedMessage message = new GameCreatedMessage();
        return message.fill(model);
    }
}
