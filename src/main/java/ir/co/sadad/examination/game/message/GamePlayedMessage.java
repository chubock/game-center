package ir.co.sadad.examination.game.message;

public class GamePlayedMessage {

    private String messageType = "GAME_PLAYED";
    private String username;
    private String move;

    public GamePlayedMessage(String username, String move) {
        this.username = username;
        this.move = move;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMove() {
        return move;
    }

    public void setMove(String move) {
        this.move = move;
    }
}
