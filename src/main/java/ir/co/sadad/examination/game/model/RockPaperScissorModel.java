package ir.co.sadad.examination.game.model;

import ir.co.sadad.examination.game.entity.RockPaperScissor;

public class RockPaperScissorModel extends TwoOpponentGameModel {

    private RockPaperScissor.Choice firstOpponentChoice;
    private RockPaperScissor.Choice secondOpponentChoice;

    public RockPaperScissor.Choice getFirstOpponentChoice() {
        return firstOpponentChoice;
    }

    public void setFirstOpponentChoice(RockPaperScissor.Choice firstOpponentChoice) {
        this.firstOpponentChoice = firstOpponentChoice;
    }

    public RockPaperScissor.Choice getSecondOpponentChoice() {
        return secondOpponentChoice;
    }

    public void setSecondOpponentChoice(RockPaperScissor.Choice secondOpponentChoice) {
        this.secondOpponentChoice = secondOpponentChoice;
    }

    void fill(RockPaperScissor game) {
        super.fill(game);
        setFirstOpponentChoice(game.getFirstOpponentChoice());
        setSecondOpponentChoice(game.getSecondOpponentChoice());
    }

    public static RockPaperScissorModel valueOf(RockPaperScissor game) {
        if (game == null)
            return null;
        RockPaperScissorModel model = new RockPaperScissorModel();
        model.fill(game);
        return model;
    }


}
