package ir.co.sadad.examination.game.message;

import ir.co.sadad.examination.game.model.TicTacToeModel;

public class TicTacToeCreatedMessage extends GameCreatedMessage {

    private String opponentTurn;

    public String getOpponentTurn() {
        return opponentTurn;
    }

    public void setOpponentTurn(String opponentTurn) {
        this.opponentTurn = opponentTurn;
    }

    TicTacToeCreatedMessage fill(TicTacToeModel model) {
        super.fill(model);
        setOpponentTurn(model.getOpponentTurn());
        return this;
    }

    public static TicTacToeCreatedMessage messageOf(TicTacToeModel model) {
        if (model == null)
            return null;
        TicTacToeCreatedMessage message = new TicTacToeCreatedMessage();
        return message.fill(model);
    }
}
