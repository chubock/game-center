package ir.co.sadad.examination.game.entity;

public enum GameType {
    ROCK_PAPER_SCISSOR,
    TIC_TAC_TOE
}
