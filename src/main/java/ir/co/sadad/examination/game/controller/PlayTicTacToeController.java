package ir.co.sadad.examination.game.controller;

import ir.co.sadad.examination.game.message.GamePlayedMessage;
import ir.co.sadad.examination.game.message.TwoOpponentGameFinishedMessage;
import ir.co.sadad.examination.game.model.TicTacToeModel;
import ir.co.sadad.examination.game.service.TicTacToeService;
import ir.co.sadad.examination.game.util.SecurityUtils;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/games/tic-tac-toes/{id}")
public class PlayTicTacToeController extends BasePlayGameController<TicTacToeModel> {

    private final SimpMessagingTemplate simpMessagingTemplate;

    public PlayTicTacToeController(TicTacToeService ticTacToeService,
                                   SimpMessagingTemplate simpMessagingTemplate) {
        super(ticTacToeService);
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @Override
    void afterGamePlayedHandler(String move, TicTacToeModel gameModel) {

        GamePlayedMessage playedMessage = new GamePlayedMessage(SecurityUtils.getCurrentUsername(), move);
        gameModel.getParticipants()
                .forEach(username -> {
                    if (!username.equals(SecurityUtils.getCurrentUsername()))
                        simpMessagingTemplate.convertAndSendToUser(username, "/queue/reply", playedMessage);
                });


        if (gameModel.isFinished()) {

            TwoOpponentGameFinishedMessage message = TwoOpponentGameFinishedMessage.messageOf(gameModel);

            gameModel.getParticipants()
                    .forEach(username -> simpMessagingTemplate.convertAndSendToUser(username, "/queue/reply", message));

        }

    }
}
