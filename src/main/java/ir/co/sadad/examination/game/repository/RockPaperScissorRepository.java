package ir.co.sadad.examination.game.repository;

import ir.co.sadad.examination.game.entity.RockPaperScissor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RockPaperScissorRepository extends JpaRepository<RockPaperScissor, Long> {
}
