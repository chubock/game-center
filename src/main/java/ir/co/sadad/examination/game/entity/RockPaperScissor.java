package ir.co.sadad.examination.game.entity;

import javax.persistence.Entity;
import javax.persistence.Enumerated;

@Entity
public class RockPaperScissor extends TwoOpponentGame {

    private Choice firstOpponentChoice;
    private Choice secondOpponentChoice;

    protected RockPaperScissor() {
    }

    public RockPaperScissor(User firstOpponent, User secondOpponent, Integer price) {
        super(firstOpponent, secondOpponent, price);
    }

    @Enumerated
    public Choice getFirstOpponentChoice() {
        return firstOpponentChoice;
    }

    private void setFirstOpponentChoice(Choice firstOpponentChoice) {
        this.firstOpponentChoice = firstOpponentChoice;
    }

    @Enumerated
    public Choice getSecondOpponentChoice() {
        return secondOpponentChoice;
    }

    private void setSecondOpponentChoice(Choice secondOpponentChoice) {
        this.secondOpponentChoice = secondOpponentChoice;
    }

    @Override
    public GameType getGameType() {
        return GameType.ROCK_PAPER_SCISSOR;
    }

    private void firstOpponentChooses(Choice choice) {
        if (firstOpponentChoice != null)
            throw new IllegalStateException();
        setFirstOpponentChoice(choice);
        if (secondOpponentChoice != null)
            findWinner();
    }

    private void secondOpponentChooses(Choice choice) {
        if (secondOpponentChoice != null)
            throw new IllegalStateException();
        setSecondOpponentChoice(choice);
        if (firstOpponentChoice != null)
            findWinner();
    }

    private void findWinner() {
        if (firstOpponentChoice == secondOpponentChoice)
            draw();
        else if (firstOpponentChoice == Choice.ROCK && secondOpponentChoice == Choice.SCISSOR)
            win(getFirstOpponent());
        else if (firstOpponentChoice == Choice.SCISSOR && secondOpponentChoice == Choice.PAPER)
            win(getFirstOpponent());
        else
            win(getSecondOpponent());
    }

    @Override
    public void firstOpponentMove(String move) {
        firstOpponentChooses(Choice.valueOf(move));
    }

    @Override
    public void secondOpponentMove(String move) {
        secondOpponentChooses(Choice.valueOf(move));
    }

    public enum Choice {
        ROCK, PAPER, SCISSOR
    }
}
