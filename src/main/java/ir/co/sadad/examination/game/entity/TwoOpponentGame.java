package ir.co.sadad.examination.game.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public abstract class TwoOpponentGame extends Game {

    private User firstOpponent;
    private User secondOpponent;
    private User winner;

    protected TwoOpponentGame() {
    }

    public TwoOpponentGame(User firstOpponent, User secondOpponent, Integer price) {
        super(price);
        setFirstOpponent(firstOpponent);
        setSecondOpponent(secondOpponent);
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    public User getFirstOpponent() {
        return firstOpponent;
    }

    private void setFirstOpponent(User firstOpponent) {
        if (firstOpponent == null)
            throw new NullPointerException();
        this.firstOpponent = firstOpponent;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    public User getSecondOpponent() {
        return secondOpponent;
    }

    private void setSecondOpponent(User secondOpponent) {
        if (secondOpponent == null)
            throw new NullPointerException();
        this.secondOpponent = secondOpponent;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public User getWinner() {
        return winner;
    }

    private void setWinner(User winner) {
        this.winner = winner;
    }

    protected void draw() {
        if (getState() == State.FINISHED)
            throw new IllegalStateException();
        super.finish();
    }

    protected void win(User winner) {
        if (getState() == State.FINISHED)
            throw new IllegalStateException();
        super.finish();
        setWinner(winner);
    }

    public abstract void firstOpponentMove(String move);
    public abstract void secondOpponentMove(String move);
}
