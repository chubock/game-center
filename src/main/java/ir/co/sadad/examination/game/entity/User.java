package ir.co.sadad.examination.game.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class User implements Serializable {

    private String username;
    private String password;
    private Long credit;

    protected User() {
    }

    public User(String username, String password, Long credit) {
        setUsername(username);
        setPassword(password);
        setCredit(credit);
    }

    @Id
    public String getUsername() {
        return username;
    }

    private void setUsername(String username) {
        if (username == null)
            throw new NullPointerException();
        if (username.isEmpty())
            throw new IllegalArgumentException();
        this.username = username;
    }

    @NotEmpty
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if (password == null)
            throw new NullPointerException();
        if (password.isEmpty())
            throw new IllegalArgumentException();
        this.password = password;
    }

    @NotNull
    @Min(0)
    public Long getCredit() {
        return credit;
    }

    private void setCredit(Long credit) {
        if (credit == null)
            throw new NullPointerException();
        if (credit < 0)
            throw new IllegalArgumentException();
        this.credit = credit;
    }

    public void addCredit(Integer amount) {
        if (amount < 0)
            throw new IllegalArgumentException();
        setCredit(credit + amount);
    }

    public void reduceCredit(Integer amount) {
        if (amount < 0)
            throw new IllegalArgumentException();
        setCredit(credit - amount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(getUsername(), user.getUsername());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getUsername());
    }
}
