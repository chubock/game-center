package ir.co.sadad.examination.game.repository;

import ir.co.sadad.examination.game.entity.GameRequest;
import ir.co.sadad.examination.game.entity.GameType;
import ir.co.sadad.examination.game.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface GameRequestRepository extends JpaRepository<GameRequest, Long> {

    @Query("select g from GameRequest g join fetch g.requester where g.id = :id")
    Optional<GameRequest> findByIdWithRequester(@Param("id") Long id);
    Optional<GameRequest> findFirstByRequesterIsNotAndGameTypeAndState(User requester, GameType gameType, GameRequest.State state);

}
