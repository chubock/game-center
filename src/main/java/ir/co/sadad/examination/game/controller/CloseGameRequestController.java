package ir.co.sadad.examination.game.controller;

import ir.co.sadad.examination.game.service.GameRequestService;
import ir.co.sadad.examination.game.util.SecurityUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class CloseGameRequestController {

    private final GameRequestService gameRequestService;

    public CloseGameRequestController(GameRequestService gameRequestService) {
        this.gameRequestService = gameRequestService;
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/api/game-requests/{id}")
    public void closeGameRequest(@PathVariable Long id) {
        gameRequestService.closeGameRequest(SecurityUtils.getCurrentUsername(), id);
    }
}
