package ir.co.sadad.examination.game.controller;

import ir.co.sadad.examination.game.entity.GameType;
import ir.co.sadad.examination.game.message.GameCreatedMessage;
import ir.co.sadad.examination.game.model.RockPaperScissorModel;
import ir.co.sadad.examination.game.service.GameRequestService;
import ir.co.sadad.examination.game.service.RockPaperScissorService;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/rock-paper-scissor-requests")
public class CreateRockPaperScissorRequestController extends BaseCreateGameRequestController<RockPaperScissorModel> {

    private final SimpMessagingTemplate simpMessagingTemplate;

    public CreateRockPaperScissorRequestController(GameRequestService gameRequestService,
                                                   RockPaperScissorService rockPaperScissorService,
                                                   SimpMessagingTemplate simpMessagingTemplate) {
        super(gameRequestService, rockPaperScissorService, GameType.ROCK_PAPER_SCISSOR);
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @Override
    void afterGameCreatedHandler(RockPaperScissorModel gameModel) {
        GameCreatedMessage message = GameCreatedMessage.messageOf(gameModel);
        gameModel.getParticipants()
                .forEach(username -> simpMessagingTemplate.convertAndSendToUser(username, "/queue/reply", message));
    }
}
