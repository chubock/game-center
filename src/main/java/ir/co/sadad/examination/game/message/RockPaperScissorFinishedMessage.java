package ir.co.sadad.examination.game.message;

import ir.co.sadad.examination.game.entity.RockPaperScissor;
import ir.co.sadad.examination.game.model.RockPaperScissorModel;

public class RockPaperScissorFinishedMessage extends TwoOpponentGameFinishedMessage {

    private RockPaperScissor.Choice firstOpponentChoice;
    private RockPaperScissor.Choice secondOpponentChoice;

    public RockPaperScissor.Choice getFirstOpponentChoice() {
        return firstOpponentChoice;
    }

    public void setFirstOpponentChoice(RockPaperScissor.Choice firstOpponentChoice) {
        this.firstOpponentChoice = firstOpponentChoice;
    }

    public RockPaperScissor.Choice getSecondOpponentChoice() {
        return secondOpponentChoice;
    }

    public void setSecondOpponentChoice(RockPaperScissor.Choice secondOpponentChoice) {
        this.secondOpponentChoice = secondOpponentChoice;
    }

    RockPaperScissorFinishedMessage fill(RockPaperScissorModel model) {
        super.fill(model);
        setFirstOpponentChoice(model.getFirstOpponentChoice());
        setSecondOpponentChoice(model.getSecondOpponentChoice());
        return this;
    }

    public static RockPaperScissorFinishedMessage messageOf(RockPaperScissorModel model) {
        if (model == null)
            return null;
        RockPaperScissorFinishedMessage message = new RockPaperScissorFinishedMessage();
        return message.fill(model);
    }
}
