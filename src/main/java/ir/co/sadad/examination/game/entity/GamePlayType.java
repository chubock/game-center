package ir.co.sadad.examination.game.entity;

public enum GamePlayType {
    INTERACTIVE,
    INDEPENDENT
}
