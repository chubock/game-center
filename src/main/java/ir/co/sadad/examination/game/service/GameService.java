package ir.co.sadad.examination.game.service;

import ir.co.sadad.examination.game.model.GameModel;

import java.util.Optional;

public interface GameService<T extends GameModel> {

    Optional<T> create(Long gameRequestId);
    T move(Long gameId, String username, String move);

}
