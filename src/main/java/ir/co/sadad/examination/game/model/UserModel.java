package ir.co.sadad.examination.game.model;

import ir.co.sadad.examination.game.entity.User;

public class UserModel {

    private String username;
    private Long credit;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getCredit() {
        return credit;
    }

    public void setCredit(Long credit) {
        this.credit = credit;
    }

    void fill (User user) {
        setUsername(user.getUsername());
        setCredit(user.getCredit());
    }

    public static UserModel valueOf(User user) {
        if (user == null)
            return null;
        UserModel model = new UserModel();
        model.fill(user);
        return model;
    }
}
