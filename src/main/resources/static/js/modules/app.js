angular.module('app', ["ngRoute"])
    .config(function ($httpProvider, $routeProvider) {

        $httpProvider.defaults.withCredentials = true;

        $routeProvider.when("/home", {templateUrl: "/views/home.html", "controller": "homeController"});
        $routeProvider.when("/loading", {templateUrl: "/views/loading.html", "controller": "loadingController"});
        $routeProvider.when("/loading/:id", {templateUrl: "/views/loading.html", "controller": "loadingController"});
        $routeProvider.when("/rock-paper-scissors/:id", {templateUrl: "/views/rock-paper-scissor.html", "controller": "rockPaperScissorController"});
        $routeProvider.when("/tic-tac-toes/:id", {templateUrl: "/views/tic-tac-toe.html", "controller": "ticTacToeController"});

        $routeProvider.otherwise({
            redirectTo: "/home"
        });

    })
    .service("userService", UserService)
    .service("socketService", SocketService)
    .service("gameRequestService", GameRequestService)
    .service("rockPaperScissorService", RockPaperScissorService)
    .service("ticTacToeService", TicTacToeService)
    .controller("ticTacToeController", TicTacToeController)
    .controller("rockPaperScissorController", RockPaperScissorController)
    .controller("loadingController", LoadingController)
    .controller("homeController", HomeController)
    .controller("appController", AppController);