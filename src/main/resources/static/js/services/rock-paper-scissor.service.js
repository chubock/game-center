function RockPaperScissorService($http) {

    function move(gameId, move) {
        return $http.patch("/api/games/rock-paper-scissors/" + gameId, {move: move})
            .then(function (value) { return value.data; });
    }

    return {
        move: move
    }


}