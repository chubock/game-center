function TicTacToeService($http) {

    function move(gameId, move) {
        return $http.patch("/api/games/tic-tac-toes/" + gameId, {move: move})
            .then(function (value) { return value.data; });
    }

    return {
        move: move
    }


}