function GameRequestService($http) {

    function createGameRequest(gameType) {
        var url = "/api/" + gameType + "-requests";
        return $http.post(url)
            .then(function (value) { return value.data });
    }

    function cancelGameRequest(id) {
        return $http.delete("/api/game-requests/" + id);
    }

    return {
        createGameRequest: createGameRequest,
        cancelGameRequest: cancelGameRequest
    }

}