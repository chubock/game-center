function UserService($http) {

    function getUser(username, password) {
        return $http.get("/me")
            .then(function (value) { return value.data });
    }

    return {
        getUser: getUser
    }

}