function SocketService ($q) {

    var socket = new WebSocket('ws://localhost:8080/greeting');
    var ws = Stomp.over(socket);

    var gameCreated = $q.defer();
    var gameFinished = $q.defer();
    var gamePlayed = $q.defer();
    var handler = function () {};

    ws.connect({}, function (frame) {
        ws.subscribe("/user/queue/reply", function (message) {
            var m = JSON.parse(message.body);
            // handler(m);
            if (m.messageType === "GAME_CREATED") {
                gameCreated.resolve(m);
                gameCreated = $q.defer();
            } else if (m.messageType === "GAME_FINISHED") {
                gameFinished.resolve(m);
                gameFinished = $q.defer();
            } else if (m.messageType === "GAME_PLAYED") {
                gamePlayed.resolve(m);
                gamePlayed = $q.defer();
            }
        })
    });

    function onGameCreated(callback) {
        // handler = callback;
        return gameCreated.promise.then(callback);
    }

    function onGameFinished(callback) {
        return gameFinished.promise.then(callback);
    }

    function onGamePlayed(callback) {
        return gamePlayed.promise.then(callback);
    }
    
    return {
        onGameCreated: onGameCreated,
        onGameFinished: onGameFinished,
        onGamePlayed: onGamePlayed
    }
}