function TicTacToe() {

    var instance = {};

    instance.columns = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    instance.myTurn = false;
    instance.state = -2;

    instance.setMyMove = function (move) {
        instance.columns[move] = 1;
        instance.myTurn = false;
    };

    instance.setOpponentMove = function (move) {
        instance.columns[move] = -1;
        instance.myTurn = true;
    };


    return instance;

}