function RockPaperScissor() {

    var instance = {};
    instance.state = -2;

    instance.setMyAction = function (action) {
        instance.myAction = action;
    };

    instance.setOpponentAction = function (action) {
        instance.opponentAction = action;
    };

    return instance;

}