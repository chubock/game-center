function HomeController($rootScope, $location, gameRequestService, socketService) {

    $rootScope.createGameRequest = function (gameType) {

        socketService.onGameCreated(function (model) {
            $rootScope.gameModel = model;
            console.log("Game Created with ID " + model.id);
            $location.path("/" + gameType + "s/" + model.id);
        });

        gameRequestService.createGameRequest(gameType)
            .then(function (data) {
                $rootScope.updateUser();
                if (!$location.path().startsWith("/" + gameType + "s/"))
                    $location.path('/loading/' + data.id);
            });

    };

    $rootScope.playAgain = function (gameType) {
            $location.path('/loading');
            $rootScope.createGameRequest(gameType);
    };
    
    $rootScope.goHome = function () {
        $location.path("/");
    }

}