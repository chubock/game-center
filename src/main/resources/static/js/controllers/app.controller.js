function AppController($rootScope, userService) {

    $rootScope.updateUser = function() {
        userService.getUser()
            .then(function (user) {
                $rootScope.user = user;
            });
    };

    $rootScope.updateUser();

}