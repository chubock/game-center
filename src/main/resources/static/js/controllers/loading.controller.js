function LoadingController($scope, $rootScope, $routeParams, $location, gameRequestService) {

    $scope.id = $routeParams['id'];

    $scope.cancel = function () {
        gameRequestService.cancelGameRequest($scope.id)
            .then(function (value) {
                $rootScope.updateUser();
                $location.path('/');
            })
    }

}