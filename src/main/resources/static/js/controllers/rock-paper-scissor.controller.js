function RockPaperScissorController($scope, $rootScope, $routeParams, $location, rockPaperScissorService, socketService) {

    var id = $routeParams['id'];
    $scope.game = RockPaperScissor();

    socketService.onGameFinished(function (result) {


        if ($rootScope.user.username === result.firstOpponent)
            $scope.game.setOpponentAction(result.secondOpponentChoice);
        else
            $scope.game.setOpponentAction(result.firstOpponentChoice);

        if (result.winner === $rootScope.user.username)
            $scope.game.state = 1;
        else if (result.winner === null)
            $scope.game.state = 0;
        else
            $scope.game.state = -1;

        $rootScope.updateUser();

    });

    $scope.select = function (choice) {
        $scope.game.setMyAction(choice);
        rockPaperScissorService.move(id, choice);
    };

}