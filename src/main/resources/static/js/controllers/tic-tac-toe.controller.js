function TicTacToeController($scope, $rootScope, $routeParams, $location, ticTacToeService, socketService) {

    var id = $routeParams['id'];
    $scope.game = TicTacToe();

    if ($rootScope.user.username === $rootScope.gameModel.opponentTurn)
        $scope.game.myTurn = true;
    else
        socketService.onGamePlayed(onOpponentMove);

    function onOpponentMove(result) {
        $scope.game.setOpponentMove(result.move);
    }
    
    socketService.onGameFinished(function (result) {
        if (result.winner === $rootScope.user.username)
            $scope.game.state = 1;
        else if (result.winner === null)
            $scope.game.state = 0;
        else
            $scope.game.state = -1;
        $scope.game.myTurn = false;
        $rootScope.updateUser();
    });

    $scope.select = function (move) {
        $scope.game.setMyMove(move);
        ticTacToeService.move(id, move);
        socketService.onGamePlayed(onOpponentMove)
    };




}