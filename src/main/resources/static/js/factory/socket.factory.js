function SocketFactory() {
    var stack = [];
    var onMessageDefer;
    var socket = {
        ws: new WebSocket("ws://localhost:8080/gs-guide-websocket"),
        send: function(data) {
            data = JSON.stringify(data);
            if (socket.ws.readyState === 1) {
                socket.ws.send(data);
            } else {
                stack.push(data);
            }
        },
        onMessage: function(callback) {
            if (socket.ws.readyState === 1) {
                socket.ws.onMessage = callback;
            } else {
                onMessageDefer = callback;
            }
        }
    };
    socket.ws.onopen = function(event) {
        for (i in stack) {
            socket.ws.send(stack[i]);
        }
        stack = [];
        if (onMessageDefer) {
            socket.ws.onMessage = onMessageDefer;
            onMessageDefer = null;
        }
    };
    return socket;
}